import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import Time from './utils/Time';
import Indicator from './components/Indicator.jsx';

//import { ipcRenderer } from 'electron'; 
/*ipcRenderer.on('dataPPS', (event, data) => {
    propsValues.items.push(data);
})*/

const time = new Time({
    hours: 3,
    minutes: 30,
    seconds: 30
});

let progress = 0;

class App extends Component {
    constructor(props) {
        super(props);

        setInterval(() => {
            time.increase(1);
            progress += 1;
            this.forceUpdate();
        }, 1000);
    }
    render() {
        return <Indicator time={time} progress={progress} />
    }
}
 
ReactDOM.render(
    <App />,
    document.getElementById("app")
)