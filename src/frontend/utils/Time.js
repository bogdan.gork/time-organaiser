import { TouchBarScrubber } from "electron";
import { reset } from "ansi-colors";

export default class Time {
  constructor({hours = 0, minutes = 0, seconds = 0}) {
    this._hours = hours;
    this._minutes = minutes;
    this._seconds = seconds;
  }

  toString() {
    return `${this._hours.toString().padStart(2, '0')}\
           :${this._minutes.toString().padStart(2, '0')}\
           :${this._seconds.toString().padStart(2, '0')}`;
  }

  reset() {
    this._hours = 0;
    this._minutes = 0;
    this._seconds = 0;
  }

  set seconds(seconds) {
    this._seconds = seconds % 60;

    if (seconds >= 60) {
      this.minutes += seconds / 60;
    }
  }

  set minutes(minutes) {
    this._minutes = minutes % 60;
    
    if (minutes >= 60) {
      this.hours += minutes / 60;
    }
  }

  set hours(hours) {
    this._hours = hours % 100;
  }

  get seconds() {
    return this._seconds;
  }
  get minutes() {
    return this._minutes;
  }
  get hours() {
    return this._hours;
  }

  increase(n) {
    this.seconds += n;
    console.log(this.toString(), this._seconds);
  }
  
  decrease(n) {
    this.seconds -= n;
  }
}