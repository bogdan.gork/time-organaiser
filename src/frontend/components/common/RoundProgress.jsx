import React, { Component } from 'react';

export default class RoundProgress extends Component {
  render() {
      return (
        <div>
          a: {this.props.value}
        </div>
      );
  }

  componentWillReceiveProps(nextProps) {
      this.forceUpdate();
  }
}