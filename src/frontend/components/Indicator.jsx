import React, { Component } from 'react';
import RoundProgress from './common/RoundProgress.jsx';
import Time from '../utils/Time';

export default class Indicator extends Component {
  constructor(props) {
    super(props);

    this.state = {
      time: props.time,
      progress: props.progress
    }
  }

  render() {
    return (
      <div class="indicator-round">
        <RoundProgress value={this.state.progress} />
        <div class="indicator-round-label">Time left</div>
        <div class="indicator-round-time">{this.state.time.toString()}</div>
      </div>
    );
  }

  componentWillReceiveProps(nextProps) {
      this.forceUpdate();
  }
}