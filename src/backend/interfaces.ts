import { BrowserWindowConstructorOptions } from 'electron';

export interface IWindowConfig {
  url: string;
  options: BrowserWindowConstructorOptions;
}

export interface IWindowsConfigList {
  MainWindow: IWindowConfig;
}

export interface IConfig {
  readonly ICON_URL: string,
  readonly WINDOWS: IWindowsConfigList
}