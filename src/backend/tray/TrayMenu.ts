import { App, Menu, MenuItem } from 'electron';
import { quitCommand } from './commands';

/**
 * An class for tray menu
 *
 * @export
 * @class TrayMenu
 */
export default class TrayMenu {
  private _app: App;
  private _menu: Menu;

  constructor(app: App) {
    this._app = app;
    this._menu = Menu.buildFromTemplate(this.items);
  }

  get items() {
    return [
      new MenuItem({
        label: 'Quit',
        click: quitCommand(this._app)
      })
    ];
  }

  get instance() {
    return this._menu;
  }
}
