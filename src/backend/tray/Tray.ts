import { Tray as ElTray, App as ElApp } from 'electron';
import TrayMenu from './TrayMenu';

export default class Tray {
  private _tray: ElTray;
  private _app: ElApp;

  constructor(app: ElApp, logoUrl: string) {
    this._tray = new ElTray(logoUrl);
    this._app = app;

    this.enableMenu();
    this.enableTooltip();
  }

  private enableMenu() {
    const trayMenu = new TrayMenu(this._app);
    this._tray.setContextMenu(trayMenu.instance);
  }

  private enableTooltip() {
    this._tray.setToolTip('This is my application');
  }

  get tray() {
    return this._tray;
  }
}