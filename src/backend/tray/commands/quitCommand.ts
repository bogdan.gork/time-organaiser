import { App } from 'electron';

/**
 * Function which instantly closes application when called
 * 
 * @param {App} [app]
 */
export default (app: App) => () => {
  app.exit();
}