import { App, ipcMain } from 'electron';
import Config from './config';
import Tray from './tray';
import MainWindow from './windows/Main';
import * as trayWindow from 'electron-tray-window';

export default class Main {
  private _app: App;
  private _config: Config;
  private _initialized: boolean = false;
  
  private _tray: Tray;

  private _mainWindow: MainWindow;

  constructor(app: App) {
    this._app = app;
    this._config = new Config();

    this.subscribeOnEvents();
  }

  private subscribeOnEvents() {
    this._app.on('activate', () => {
      if (!this._initialized) {
        this.initializeWindows();
      }
    })

    this._app.once('ready', () => {
      this.initializeWindows();
      this.initializeTray();

      console.log('App is ready');
    });
  }

  private initializeWindows() {
    this._mainWindow = new MainWindow(this._config.WINDOWS.MainWindow);

    this._initialized = true;

    this._mainWindow.window.once('show', () => {
      this._mainWindow.window.webContents.send('dataPPS', 'Hello world');
    })

    ipcMain.on('report-data', (event, data) => {
      console.log(data);
      this._mainWindow.window.webContents.send('dataPPS', data);
    })
  }

  private initializeTray() {
    console.log(this._config.ICON_URL);
    this._tray = new Tray(this._app, this._config.ICON_URL);

    trayWindow.setOptions({ tray: this._tray.tray, window: this._mainWindow.window });
  }
}