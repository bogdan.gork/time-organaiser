import { BrowserWindow } from 'electron'
import { IWindowConfig } from '../../interfaces'

export default class MainWindow {
  private _win: BrowserWindow;
  private _config: IWindowConfig;

  constructor(config: IWindowConfig) {
    this._win = new BrowserWindow(config.options);
    this._config = config;

    this.load();
  }

  private load() {
    this._win.loadFile(this._config.url);
  }

  public get window() {
    return this._win;
  }
}
