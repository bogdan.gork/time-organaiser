import { IConfig, IWindowsConfigList } from '../interfaces';
import * as config from './settings.json';
import * as path from 'path';
import { readdirSync } from 'fs';

const getDirectories = source =>
  readdirSync(source, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name)

export default class Config implements IConfig {
  get ICON_URL() : string {
    return config.logoUrl || 'icon.ico';
  }
  
  get WINDOWS() : IWindowsConfigList {
    let mainWindowConfig = config.windows.main;

    return {
      MainWindow: {
        url: mainWindowConfig.url,
        options: {
          width: mainWindowConfig.width | 0,
          height: mainWindowConfig.height | 0,
          webPreferences: {
            nodeIntegration: mainWindowConfig.webPreferences.nodeIntegration
          },
          frame: mainWindowConfig.frame,
          show: mainWindowConfig.show,
          resizable: mainWindowConfig.resizable,
          movable: mainWindowConfig.movable,
          alwaysOnTop: mainWindowConfig.alwaysOnTop,
          title: mainWindowConfig.title,
          transparent: mainWindowConfig.transparent,
        }
      }
    };
  }
};

