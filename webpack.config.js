const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

const backend = {
  entry: "./src/backend/App.ts",
  output: {
    path: path.join(__dirname, 'dist'),
    filename: "app.js",
  },
  module: {
    rules: [{
      test: /\.ts$/,
      exclude: [/node_modules/],
      loader: "ts-loader"
    }]
  },
  resolve: {
    modules:[
      "node_modules"
    ],
    extensions: [".ts", ".js", ".json"]
  },
  target: "electron-main",
  node: {
    __dirname: true,
    __filename: false
  },  
  plugins: [
    new CopyPlugin([
      { from: 'public', to: 'public' },
    ]),
  ],
}

const frontend =  {
  entry: "./src/frontend/app.jsx",
  target: 'electron-renderer',
  output:{
      path: path.resolve(__dirname, 'dist', 'public'),
      publicPath: '/public/',
      filename: "bundle.js"
  },
  module:{
      rules:[
          {
              test: /\.jsx?$/,
              exclude: /(node_modules)/,
              loader: "babel-loader",
              options:{
                  presets:["@babel/preset-env", "@babel/preset-react"]
              }
          }
      ]
  },
}

module.exports = [ frontend, backend ];